import axios from "axios";
import { NODE_ENV } from "../constants";

export const BASE_URL = 'https://zakaa-gateway.onrender.com';

export const apiClient = axios.create({
    baseURL: BASE_URL
});
